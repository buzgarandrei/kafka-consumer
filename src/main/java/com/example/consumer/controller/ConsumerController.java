package com.example.consumer.controller;

import com.example.consumer.model.User;
import com.example.consumer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ConsumerController {

    private UserRepository userRepository;

    @Autowired
    public ConsumerController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping(value = "users")
    public List<User> userList () {

        List<User> userList = new ArrayList<>();
        userRepository.findAll().forEach(user -> userList.add(user));
        return userList;
    }
}
