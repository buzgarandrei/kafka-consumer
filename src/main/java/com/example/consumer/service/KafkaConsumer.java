package com.example.consumer.service;

import com.example.consumer.model.User;
import com.example.consumer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    private UserRepository userRepository;

    @Autowired
    public KafkaConsumer(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @KafkaListener(topics = "topic", groupId = "group", containerFactory = "userConcurrentKafkaListenerFactory")
    public void consumeUser(User user) {
        userRepository.save(user);
        System.out.println("consumed user: " + user.getFirstName());
    }
}
